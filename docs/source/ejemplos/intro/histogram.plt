A = 'pink'
B = 'blue'
C = 'turquoise'
D = 'light-salmon'
E = 'sea-green'
set term png size 400, 500
set yrange[0:15]
set output 'histogram.png
set boxwidth 0.9
set style data histograms
set style histogram rowstacked
set style fill solid
set key horizontal
plot 'histogram' using 1 lt rgb A title 'A',\
'' using 2 lt rgb B title 'B',\
'' using 3 lt rgb C title 'C',\
'' using 4 title 'D',\
'' using 5 lt rgb E title 'E',\
'' using 6:xtic(7) title 'F'

