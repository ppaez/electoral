============
Introducción
============

electoral es una biblioteca de funciones para leer los resultados de
las elecciones de diputados federales de 2018 y 2021 y generar una
gráfica de cada uno de los 300 distritos, así como documentos en
Markdown que referencían a las imágenes.  Los documentos en Markdown
se pueden convertir a HTML para utilizarse en un sitio web.

Ejemplos de gráficas
====================

Se muestran las gráficas de tres distritos en las elecciones del 2021.

- Una coalición

.. image:: ejemplos/01-01.png
   :width: 75%

- Dos coaliciones

.. image:: ejemplos/02-01.png
   :width: 75%

- Sin coaliciones

.. image:: ejemplos/31-01.png
   :width: 75%

Archivos de datos
=================

Los datos se toman de los archivos de resultados de elecciones
federales de 2018 y 2021 publicados por el `Instituto Nacional
Electoral <https:/ine.mx>`_ en su sitio web.


Generación de las gráficas
==========================

Para generar las gráficas se utiliza `gnuplot
<http://www.gnuplot.info/>`_.  Tenemos la siguiente versión
simplificada de las gráficas que se producen:

.. image:: ejemplos/intro/histogram.png

La gráfica tiene cuatro barras, cada una con su etiqueta al pie, y
están ordenadas de mayor a menor altura.

La primera barra `cand 1` consta de tres segmentos apilados, cada uno
con su color, y ordenados de abajo hacia arriba por su tamaño en forma
descendente. Las demás barras solamente tienen un segmento.

Todos los segmentos son únicos y aparecen una sola vez.
Hasta arriba hay una leyenda con un texto y el color que corresponde
a cada segmento, que en total con 6.

Este es un caso específico de gráfica de barras apiladas, porque
generalmente los segmentos sí se repiten en dos o mas barras.
	   
La gráfica se produjo a partir de este archivo de datos:

.. literalinclude:: ejemplos/intro/histogram

Cada renglón da los valores para cada una de las barras en la gráfica,
en este caso son cuatro renglones y hay cuatro barras en la gráfica.

El renglón da los valores de 6 posibles segmentos para esa barra y una
cadena al final que se usa para el texto debajo de cada barra, su
etiqueta.


Archivo de comandos para gnuplot:

.. literalinclude:: ejemplos/intro/histogram.plt
   :language: gnuplot

El comando para producir la gráfica es::

  gnuplot histogram.plt


Proceso
=======

*Pasos para leer y generar las gráficas*

Uso
===

*Desde la línea de comando*


Referencia
==========

electoral
=========

.. automodule:: electoral.electoral
   :members:
   :undoc-members:


core
====

.. automodule:: electoral.core
   :members:
   :undoc-members:
