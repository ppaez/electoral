.. electoral documentation master file, created by
   sphinx-quickstart on Thu Mar 14 12:30:32 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

¡Bienvenido a la documentación de electoral!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Índice:

   introduccion.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
