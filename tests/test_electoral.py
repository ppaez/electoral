import unittest


class Coalicion_a_Partidos(unittest.TestCase):

    def test_individual(self):
        from electoral.electoral import coalicion_a_partidos

        votos = {'a': 2 }
        expected = [('a', 2)]

        self.assertEqual(expected, coalicion_a_partidos(votos))


    def test_combinacion(self):
        from electoral.electoral import coalicion_a_partidos

        votos = {'a-b': 2 }
        expected = [('a', 1), ('b', 1)]

        self.assertEqual(expected, coalicion_a_partidos(votos))


    def test_combinacion_individual_falta(self):
        from electoral.electoral import coalicion_a_partidos

        votos = {'a-b': 2, 'a': 2 }
        expected = [('a', 3), ('b', 1)]

        self.assertEqual(expected, coalicion_a_partidos(votos))


    def test_combinacion_individual(self):
        from electoral.electoral import coalicion_a_partidos

        votos = {'a-b-c': 3, 'a': 2, 'b': 1, 'c': 1 }
        expected = [('a', 3), ('b', 2), ('c', 2)]

        self.assertEqual(expected, coalicion_a_partidos(votos))


    def test_sobrantes(self):
        from electoral.electoral import coalicion_a_partidos

        votos = {'a-b-c': 5, 'a': 2, 'b': 1, 'c': 1 }
        expected = [('a', 4), ('b', 3), ('c', 2)]

        self.assertEqual(expected, coalicion_a_partidos(votos))

