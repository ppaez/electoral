{gnuplot_variables}
set term png size 800, 1100
set decimalsign locale; set decimalsign "."
set format "%'.0f"
set yrange[0:270000]
set output '{image_name}'
set key left
set boxwidth 0.9
set style data histograms
set style histogram rowstacked
set style fill solid
set key horizontal
set xtics rotate nomirror
set xlabel 'Candidaturas'
set mytics
set ylabel 'Votos'
set title "{nombre_entidad} Distrito {distrito} {nombre_distrito} {{/:Bold {participacion:.1f}%}} participación {{/:Bold {lista:,d}}} Lista Nominal"
plot {plot_parameters}

