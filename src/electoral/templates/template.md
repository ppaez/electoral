---
title: "Elecciones 2021"
date: 2021-06-23T10:46:05-05:00
tags: []
draft: false
toc: true
---

¿Quién ganó en mi distrito para diputaciones federales?
¿Quienes quedaron en segundo y tercer lugar?
¿Qué tantos votantes y abstencionistas hubo en mi distrito?

Consulta estas y otras preguntas en los resulados en cada uno de los
300 distritos.

<!--more-->

## Introducción

A continuación se muestran gráficamente los resultados por distrito de
las votaciones de diputaciones federales de 2021 con los nombres de
las candidaturas que compitieron, para analizar, comparar y aprender.

Los datos se obtuvieron de la [Base de
datos](https://computos2021.ine.mx/base-de-datos) de los Cómputos
Distritales 2021, la cual está disponible en el sitio del INE.

## Sugerencias para el análisis

Se puede comenzar analizando el distrito donde el lector votó, después
los distritos del estado y otros estados, comparando las diferencias.

Se sugiere plantear entre otras estas preguntas:

- ¿Cómo se llama la candidatura que ganó en este distrito?

- ¿Qué tan cerca o lejos estuvo el ganador de los otros competidores?
  ¿Hay una diferencia grande o pequeña entre los votos obtenidos por
  cada competidor?

- Si se compitió en coalición:

  - ¿Hay mucha o poca diferencia entre los votos recibidos por cada
    partido miembro de la coalición?

  - ¿Qué partido de la coalición recibió más votos?  ¿Cuál aportó menos
    votos?

  - ¿Se pudo haber ganado *sin* la coalición, solamente con los votos del
    partido más fuerte?

- En relación a la cantidad de abstencionistas:

  - Si los abstencionistas fueran un partido ¿Habrían ganado ese
    distrito?

  - Si algunos de los abstencionistas se hubieran animado a votar por
    el segundo o el tercer candidato ¿Podrían haber cambiado el resultado
    del distrito?  En caso afirmativo:

    ¿Cuántos votos de
    abstencionistas se necesitarían para ese cambio en el resultado y
    qué tantos son del total?

- ¿En qué lugar quedaron los votos nulos?

- ¿Cuáles partidos obtuvieron menos votos que la cantidad de votos
  nulos?

## Explicación de las gráficas


Hay una gráfica por cada uno de los 300 distritos y están ordenadas
por entidad federativa.  Al inicio de esta página hay un índice para
fácil acceso.

En cada gráfica se muestra:

- El título con la entidad federativa, el número y el nombre del
  distrito, el porcentaje de participación y la cantidad de votantes
  en la lista nominal.

- La leyenda con los colores y nombres de los partidos que compitieron
  en el distrito. Se usan los mismos colores que utiliza el INE en la
  sección de Cómputos Distritales 2021.

- En la izquierda hay barras con el nombre de cada candidatura y los
  votos totales que obtuvo, ordenadas de izquierda a derecha por la
  cantidad de votos de mayor a menor.

  Se incluyen una barra para los votos nulos y otra para los votos por
  candidaturas no registradas.

  Si la barra tiene más de un color, se trata de una coalición.  Los
  partidos de esa coalición están ordenados por cantidad de votos
  obtenidos de abajo hacia arriba.

- Hasta la derecha hay dos barras: La primera en verde claro muestra
  el total de votos del distrito y corresponde a la suma de todas las
  barras que están a su izquierda. La segunda barra en gris muestra la
  cantidad de personas que se abstuvieron de votar.

{text}
