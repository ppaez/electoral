---
title: {nombre_entidad} Distrito {distrito} {nombre_distrito}
slug: {entidad:0>2}-{distrito:0>2}
date: 2021-06-24T10:46:05-05:00
tags: []
draft: false
---

![](/img/{entidad:0>2}-{distrito:0>2}.png)

[Ver los 300 distritos](/docs/elecciones-2021)
