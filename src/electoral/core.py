import csv


def load_candidatura(csv_file_path):

    candidatura = {}
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter='|')
        next(reader)
        for estado, distrito, partido, propietario, suplente in reader:
            propietario = propietario.title()
            suplente = suplente.title()
            if estado not in candidatura:
                candidatura[estado] = {}
            if distrito not in candidatura[estado]:
                candidatura[estado][distrito] = {}
            if propietario not in candidatura[estado][distrito]:
                candidatura[estado][distrito][propietario] = [{}, suplente]
            if partido not in candidatura[estado][distrito][propietario][0]:
                candidatura[estado][distrito][propietario][0][partido] = 0
    return candidatura

def load_votos(csv_file_path):

    estados = {}
    distritos = {}
    votos = {}
    with open(csv_file_path, newline='') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='|')
        for row in reader:
            estado = row['ID_ESTADO']
            distrito = row['ID_DISTRITO']
            if estado not in votos:
                votos[estado] = {}
                estados[estado] = row['NOMBRE_ESTADO']
                distritos[estado] = {}
            if distrito not in votos[estado]:
                votos[estado][distrito] = {}
                distritos[estado][distrito] = row['NOMBRE_DISTRITO']
            del row['ID_ESTADO']
            del row['NOMBRE_ESTADO']
            del row['ID_DISTRITO']
            del row['NOMBRE_DISTRITO']
            row = { key: int(value) for key, value in row.items()}
            votos[estado][distrito].update(row)
    return estados, distritos, votos


def add_votos(candidatura, votos):

    for estado in candidatura:
        for distrito in candidatura[estado]:
            totales = votos[estado][distrito]
            lista = totales['lista']
            votos_emitidos = totales['votos']
            total = sum({key: value for key, value in totales.items()
                         if key not in ['votos', 'lista']}.values())
            for propietario in candidatura[estado][distrito]:
                partidos = candidatura[estado][distrito][propietario][0]
                for partido in partidos:
                    partidos[partido] = totales.get(partido, -1)
                nvotos = sum(partidos.values())
