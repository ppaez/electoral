import re
import csv

colores = {
    'VXM': '0x87ceeb',
    'JHH': '0xf2908f',
    'MRN': '0xb45858',
    'MC': '0xffa500',
    'PAN': '0x0057b7',
    'PRI': '0x009739',
    'PRD': '0xffcd00',
    'PVEM': '0x7acc00',
    'PT': '0xe73c3e',
    'PES': '0x642667',
    'RSP': '0xa6192e',
    'FXM': '0xef60a3',
    'PNA': '0x00a6b4',
    'noreg': '0xc000ff',
    'CI': '0x707070',
    'Nulos': '0x555555',
    'Abstenciones': '0xdddddd',
    'Votos': '0x88ff88'
}


def coalicion_a_partidos(partidos):
    ''
    lista = []
    for key, value in partidos.items():
        names = key.split('-')
        for name in names:
            if name not in partidos:
                lista.append([[name], 0])
        lista.append([names, value])

    lista.sort(key=lambda i: i[1], reverse=True)
    lista.sort(key=lambda i: len(i[0]))
    
    names = [item[0][0] for item in lista if len(item[0])==1]
    
    for item in lista:
        if len(item[0])>1:
            item[0].sort(key=lambda name: names.index(name))
            votos = item[1]//len(item[0])
            sobrantes = item[1] % len(item[0])
            for name in item[0]:
                lista[names.index(name)][1] += votos
                if sobrantes:
                    lista[names.index(name)][1] += 1
                    sobrantes -= 1

    lista = [(names[0], votos) for names, votos in lista if len(names)==1]
    return lista


def display_candidaturas_votos(candidatura, votos, estados, distritos):

    tmpl = '{:45} {:10,} {}'
    partidos_tmpl = '{:7,} {}'
    for estado in candidatura:
        print(estado, estados[estado], '='*80)
        for distrito in candidatura[estado]:
            totales = votos[estado][distrito]
            lista = totales['lista']
            votos_emitidos = totales['votos']
            print(distrito, distritos[estado][distrito], votos_emitidos,
                  lista, votos_emitidos/lista, '-'*40)
            for propietario in candidatura[estado][distrito]:
                partidos = candidatura[estado][distrito][propietario][0]
                print()
                if len(partidos) == 1:
                    partidos_str = list(partidos.keys())[0]
                else:
                    partidos_str = ''
                    for partido in partidos:
                        partidos_str += partidos_tmpl.format(partidos[partido], partido)
                print(tmpl.format(propietario, sum(partidos.values()), partidos_str))
            print()


def create_data_file(data_file_name, data_tmpl, candidaturas_votos):

    columns = sum([len(partidos) for propietario, partidos in candidaturas_votos])
    data_file = open(data_file_name, 'w')
    column = 0
    for propietario, partidos_votos in candidaturas_votos:
        l = [0] * columns
        for partido, votos in partidos_votos:
            l[column] = votos
            column += 1
        str_votos = ''.join([f'{i:8}' for i in  l])
        data = data_tmpl.format(str_votos, partido, propietario)
        data_file.write(data)


def create_label_file(label_file_name, data_tmpl, candidaturas_votos):

    label_file = open(label_file_name, 'w')
    for propietario, partidos_votos in candidaturas_votos:
        total = 0
        for partido, votos in partidos_votos:
            total += votos
        data = '{:8}\n'.format(total)
        label_file.write(data)


def create_command_file(cmd_file, cmd_template, plot_tmpl, label_tmpl,
                        data_file_name, image_name,
                        nombre_entidad, distrito, nombre_distrito,
                        participacion, lista, candidaturas_votos,
                        label_file_name):

    output = open(cmd_file, 'w')
    plot_parameter_list = []
    plot_parameters = ''
    columns = sum([len(partidos) for propietario, partidos in candidaturas_votos])
    option = f':xtic({columns+2})'
    gnuplot_variables = ''
    column = 1
    for propietario, partidos_votos in candidaturas_votos:
        for partido, votos in partidos_votos:
            if column > 1:
                data_file_name = ''
            if re.search('[a-z]', partido):
                title = 'notitle'
            else:
                title = f"title '{partido}'"
            plot_parameter = plot_tmpl.format(data_file_name=data_file_name, column=column,
                                              option=option, color=partido.lower(),
                                              title=title)
            option = ''
            column += 1
            plot_parameter_list.append(plot_parameter)
            gnuplot_variables += f'{partido.lower()} = {colores.get(partido, "0x707070")}\n'
    plot_parameter_list.append(label_tmpl.format(label_file_name=label_file_name))
    plot_parameters = ', '.join(plot_parameter_list)
    cmd = cmd_template.format(**vars())
    output.write(cmd)


def create_gnuplot_files(candidatura, votos, estados, distritos, año,
                         cmd_template, plot_tmpl, label_tmpl,
                         image_folder, image_path_tmpl,
                         cmd_file_path_tmpl,
                         data_file_name_tmpl, label_file_name_tmpl,
                         data_tmpl):

    for entidad in candidatura:
        for distrito in candidatura[entidad]:
            totales = votos[entidad][distrito]
            lista = totales['lista']
            votos_emitidos = totales['votos']
            abstenciones = lista - votos_emitidos
            data_file_name = data_file_name_tmpl.format(entidad=entidad,
                                                        distrito=distrito)
            label_file_name = label_file_name_tmpl.format(entidad=entidad,
                                                        distrito=distrito)
            candidaturas_votos = []
            for propietario in candidatura[entidad][distrito]:
                partidos = candidatura[entidad][distrito][propietario][0]
                lst_partidos = coalicion_a_partidos(partidos)
                candidaturas_votos.append((propietario, lst_partidos))
            candidaturas_votos.append(('Nulos', [['Nulos', totales['nulos']]]))
            candidaturas_votos.append(('Candidaturas no registradas', [['noreg', totales['noreg']]]))
            candidaturas_votos.sort(key=lambda lst: sum([int(v) for p, v in lst[1]]), reverse=True)
            candidaturas_votos.append(('Votos totales',  [['Votos',  votos_emitidos]]))
            candidaturas_votos.append(('Abstenciones', [['Abstenciones', abstenciones]]))
            create_data_file(data_file_name, data_tmpl, candidaturas_votos)
            create_label_file(label_file_name, data_tmpl, candidaturas_votos)
            image_name = image_folder + image_path_tmpl.format(
                entidad=entidad, distrito=distrito, año=año)
            nombre_entidad = estados[entidad].title()
            nombre_distrito = distritos[entidad][distrito].title()
            participacion = votos_emitidos/lista*100
            cmd_file = cmd_file_path_tmpl.format(entidad=entidad, distrito=distrito)
            create_command_file(cmd_file,
                                cmd_template, plot_tmpl, label_tmpl,
                                data_file_name, image_name,
                                nombre_entidad, distrito, nombre_distrito,
                                participacion, lista, candidaturas_votos,
                                label_file_name)


def create_main_markdown(candidatura, votos, estados, distritos,
                         md_template, output_md_name,
                         single_page_header_tmpl, single_page_tmpl):

    md = ''
    for entidad in candidatura:
        nombre_entidad = estados[entidad].title()
        md += single_page_header_tmpl.format(nombre_entidad=nombre_entidad)
        for distrito in candidatura[entidad]:
            nombre_distrito = distritos[entidad][distrito].title()
            md += single_page_tmpl.format(entidad=entidad, distrito=distrito,
                                          nombre_distrito=nombre_distrito)
            output = open(output_md_name, 'w')
    md = md_template.format(text=md)
    output.write(md)


def create_single_markdown(candidatura, votos, estados, distritos, año,
                           md_template, output_md_name_tmpl,
                           image_path_tmpl):

    for entidad in candidatura:
        nombre_entidad = estados[entidad].title()
        for distrito in candidatura[entidad]:
            nombre_distrito = distritos[entidad][distrito].title()
            output_md_name = output_md_name_tmpl.format(entidad=entidad,
                                                        distrito=distrito,
                                                        año=año)
            output = open(output_md_name, 'w')
            md = md_template.format(**vars()).format(**vars())
            output.write(md)
