from electoral import load_candidatura
from electoral import load_votos
from electoral import add_votos
from electoral import create_gnuplot_files
from electoral import create_main_markdown
from electoral import create_single_markdown


candidatura = load_candidatura('candidaturas.csv')
estados, distritos, votos = load_votos('votos.csv')
add_votos(candidatura, votos)
# display_candidaturas_votos(candidatura, votos, estados, distritos)
create_gnuplot_files(candidatura, votos, estados, distritos)
create_main_markdown(candidatura, votos, estados, distritos)
create_single_markdown(candidatura, votos, estados, distritos)
